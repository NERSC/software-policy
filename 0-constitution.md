# Software Policy Committee Constitution

This document describes the decision-making process for changes to the NERSC
Software Policy

1. The Software Policy Committee consists of representatives from the following
   NERSC groups: UEG, APG, DAS, DSEG and CSG (the "core groups"). At least one
   voice from each core group consents to each policy addition or modification.
   Representatives from ISG and OTG are invited to contribute but are not
   required to approve decisions. (see Note 1)

2. We aim to get full consensus on each decision, and for significant changes 
   will allow time for stakeholders to be heard. Decisions are made via a Merge 
   Request to the active policy, approved by 1 member from each of the core groups.

3. The active policy will be at the HEAD of the master branch of 
   <https://gitlab.nersc.gov/nersc/consulting/policy>. Discussion can occur via
   the `#software` Slack channel, the team drive at 
   <https://drive.google.com/drive/folders/1kwyRn90suP3f69ypI6jzxsoGvAGSIhd5>
   and/or during meetings. Changes to the policy planned for the next ERCAP year
   will be prepared in the devel branch, which will be merged to the master branch
   at the year rollover. 

4. The policy includes documents detailing each of the following topics:

    1. The goals, priorities, and intent which guides this policy.
    2. Where and how we make software available to users.
    3. The levels of support we offer and their characteristics.
    4. The processes to set and change the support level of a package.
    5. Our use of testing to verify the build recipe, functionality, and 
       performance of supported software.
    6. How we measure and report the status of NERSC-provided software.

5. This policy does not cover support related to system functionality or
   auxiliary infrastructure components. These exclusions include but are not
   limited to:

   * Operating system tools, commands, utilities, and functions.
   * Network communications.
   * System job scheduling.
   * Storage system operation.
   * Service hosting infrastructure.

6. NERSC will make best effort to follow this policy, but reserves the right to
   make exceptions which address cybersecurity incidents or system capability
   regressions.

Note 1. Software policy is not expected to significantly impact ISG or OTG.
We welcome participation from these groups and intend to seek their advice 
when appropriate but we don't require them to review and approve each decision.

