# Goals and priorities guiding software policy decisions

Goals for the NERSC software support policy are listed here, ordered by
descending priority:

1. **Accessible: public, easy to read and adhere to**.
   Short, simple, implementable, key points are highlighted. A NERSC consultant
   can easily find how much effort a request warrants, and users can easily see which 
   packages are better supported than others.

2. **Make supporting software easier, rather than providing excuses for non-support**.
   Should offer direct answers to "how?" and "where?" questions and reasonable
   "Why not?" justifications when support is to be limited or unavailable.

3. **Provide certainty over a time window**.
   Users should get sufficient time to mitigate any reduction of support for 
   software they depend on.

4. **Balance user needs and NERSC staff support commitments**.
   Aim to successfully provide better support but not to unsustainably expand 
   staff commitments beyond capacity.

5. **Measurable and reportable**.
   Have the capability to measure and report the degree to which NERSC
   satisfies software support commitments.

