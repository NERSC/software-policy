# Installation and access

**NERSC will provide instructions and artifacts to reproduce the
installation procedure for supported software**, to the extent that 
the information is not confidential. "Artifacts" might include 
Makefiles, Spack specs, Dockerfiles, `configure` options, etc.

## Access

Supported software that NERSC provides via environment modules will
be in the default `$MODULEPATH`. Accessing other software available on 
the system may require the user to append another location to their 
`$MODULEPATH`, eg via a `module use` or `module load` command.

## Paths
 
Software installed by NERSC for wide availability will normally be placed
under `/global/common/software/nersc/`. 

The installation path of a provided software will not change while that 
software is supported. No-longer-supported software may, however, be 
relocated or removed.

