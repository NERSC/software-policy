# NERSC Software Environment Policy

The categories of support described in this document are intended to provide
the means to balance user software environment stability, user access to new
and upgraded software, NERSC staff support effort, user development effort,
system administration flexibility, and institutional strategic goals.

Application software at NERSC is categorized into four levels of support.
Support levels for a package may vary depending on particular package versions
or NERSC systems.

Throughout this policy the term **Supported** refers to software in either the
**Provided** or **Priority** categories. 


## Support Level Descriptions

* **Restricted** software may not be used, installed, or present on any NERSC
system without written approval from NERSC. 

* **Minimal** support software is installed and maintained by users with no direct
support or reliability commitment from NERSC. NERSC will assist users to the 
extent we are able, but may not have the expertise or resources to answer 
questions or solve problems with this software. Software not explicitly 
placed in another support category gets minimal support level. 

* **Provided** software is made available by NERSC staff with the
goal that it be accessible by users and function correctly on NERSC production
systems. Automated testing is used to verify functionality. If test outcomes,
staff investigation, or user communication indicate the package is not functional, 
NERSC staff will begin working to restore function within three business days.
**Provided** support does not include a performance guarantee.

* **Priority** software support includes consideration of performance, and 
adds more urgent support to the characteristics of **Provided**
software. When test outcomes, staff investigation, or user communication
suggest there is a problem with software functionality or performance, the
issue will receive NERSC staff attention within one business day. 

 
## Example Software Characteristics For Each Support Level

* **Restricted** software can be limited for reasons including:
  - Legal matters such as licensing or copyright
  - Technical concerns such as known security vulnerabilities or danger to the 
    operation of NERSC systems
  - Processes which violate the NERSC Appropriate Use Policy
  Additionally, NERSC does not allow the use of:
  - Classified or controlled military defense information
  - Export controlled or [ITAR](https://www.pmddtc.state.gov/ddtc_public?id=ddtc_kb_article_page&sys_id=%2024d528fddbfc930044f9ff621f961987) 
    codes or data
  - Personally identifiable information
  - Protected health information
  Any software meeting these definitions is disallowed and automatically considered 
  Restricted.

* **Minimal** support is for applications fitting some of the following descriptions:
low total compute usage, low total number of users, new and unknown, under
development, in an unstable state, licensed in a way which cannot be easily
satisfied in the NERSC environment, depreciated, no longer maintained,
requiring unsupported dependencies, poor performance or system compatibility
relative to alternatives, or requiring an excessive staff effort to support. 

* **Provided** software packages can include simple utilities broadly used at
NERSC, have a low computational footprint, infrequently run, easy to support,
convenient but not necessary for users, or dependencies needed to use other
supported software.

* **Priority** applications meet some of the following criteria: a large system
usage footprint, a large number of users, important optimizations tuned for
NERSC systems, necessary for the development and building of other
applications, or identified as strategically important by NERSC or the Department
of Energy.


## Commitments

NERSC will publish and maintain the policy state of all supported
software including the following details:
* software name and version
* current support level
* current status of this software at NERSC, including known problems
* if a support level reduction is planned, and it's effective date
* the history of previous support level changes
* if and how support levels are different on different NERSC systems
* a list of ways supported software can be accessed
* information about any tests used to validate the software 

NERSC will use testing (see 5-testing.md) and user communication to 
verify that supported software meets user needs for functionality and performance.

