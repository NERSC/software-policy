# Processes for setting and changing support levels

The default support level for any allowed[^1] software is **Minimal**. All changes to a
software support level require approval from relevant NERSC groups. Users must 
be notified of a decrease in support level by announcing a "Planned change" according to 
the support description schema (ref 3-levels.md). The "Planned change" must be announced
**at least 6 months** before the decrease takes effect unless the software is being
declared **Restricted**. 

To increase the support level of a package, the following criteria must be met:

 - An installation of the package must already exist that meets the support 
   commitments for the target level. 

   E.g. to increase from **Minimal** to **Provided** the version in question
   must have been successfully installed and tested. To increase to **Priority** 
   the performance must be verified as acceptable and necessary documentation
   produced.

 - A viable plan to meet the ongoing support commitments for the new support level must 
   exist.

   E.g. a point-of-contact for the package should be named and agreed upon, and
   should be reasonable confidence that future problems can be mitigated.

 - Reasons for the increase must be recorded in the "justification" field 
   of the support schema.
   
   E.g. "N users have requested this package", "it is used for N% of the available
   NERSC-hours", etc.


To decrease the support level of a package the following criteria must be met:

 - Justifications for the decrease must be recorded. 

   E.g. "usage has reduced to only N%", "an alternative package x is preferred", 
   "this list of software characteristics make it poorly suited for use on 
   the system", "We have insufficient expertise or resources to effectively 
   support this software at this level", etc.

 - The planned change must be communicated to users at least 6 months before the
   change takes place (unless the software is to be declared **Restricted**).

[^1]: See the discussion on Restricted software in [3-levels.md](3-levels.md).
