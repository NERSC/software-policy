# Software Testing Policy

The goals of NERSC software testing are to increase confidence that supported
software is working correctly and to detect problems in supported software
before users are impacted.

## Automated Testing

NERSC uses automated testing processes on both test and production systems.

All software may have tests written and added to the test system to
confirm package function and performance. Attributes a test may be flagged with 
include:

1. **Info** All software can be examined using **Info** tagged tests. NERSC staff may
   opt in to be notified if a specific **Info** test fails. When a software package
   is downgraded to **Minimal** its test tags will be converted to 
   **Info**.
2. **Alert** Only supported software can be tagged with **Alert**. NERSC staff are
   notified if an **Alert** test fails, and the test outcome will be reported in the
   software status if the failure occured on a production system.

Test batteries may be triggered by individual NERSC staff, before releasing a
system from maintenance, and/or on a regularly occuring schedule. Batteries run
on NERSC's test or production systems and may be limited to tests which possess
certain tags.

Information about testing regimes and outcomes will be made available for users.

