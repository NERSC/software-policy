# Software support status and metrics

NERSC will record changes to the status of Provided software based on testing
results, user reports, changes made by NERSC, and other information. The record of
software status will be publicly available, and will be used by NERSC for
reporting and decision-making.

