# Future goals

Several elements of this policy are intended as a step towards the software 
support we would like to reach. Those longer-term goals are described here.


## Metrics

One eventual goal for metric collection is to be able to include a software support 
metric in our annual Operational Assessment Report. 
 
