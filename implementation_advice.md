# Implementation advice

The notes in this document are not part of the policy, but are intended 
to help NERSC staff to implement the policy in a consistent way.

## Building software with Spack

When using Spack to install software, first verify the Spack spec by building
it with your own spack instance in your own user account. Then submit a MR to
the master branch of:
 
   ```
   gitlab.nersc.gov/NERSC/consulting/spack-provided
   ```
   or
   ```
   gitlab.nersc.gov/NERSC/consulting/spack-unstable
   ```
   according to the support level of the package.

The CI system will then build the spec in the target location.

## Automated testing

We are using Reframe for automated testing. Tests should be added to the repository 
at https://gitlab.nersc.gov/nersc/consulting/reframe-at-nersc/reframe-nersc-tests .

Tests can be flagged with the following attributes:

1. Info. All software can be examined using Info tagged tests. NERSC staff may
   opt in to be notified if a specific Info test fails. If a software package
   is downgraded to **Minimal** all of its tests automatically convert to 
   Info.
2. Alert. Supported software can include tests
   marked as Alert. NERSC staff are notified if an Alert test fails. If such a
   test fails on a production system then support for that software is
   reported as breached.


